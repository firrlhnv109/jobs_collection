package src.Class2

import src.Class.*

class RecordItem(id: Long,
                 title: String,
                 content: String,
                 var recordFile: String): Item(id, title, content){

    override fun getDetails() = "${super.getDetails()}, recordFile=$recordFile"

}

// Class array

/*fun main(args: Array<String>){
    val items: Array<Item> = arrayOf(
        Item(1, "Hi", "Jan"),
        Item(2, "Hi", "Feb"),
        Item(3, "Hi", "Mar"),
        ImageItem(11, "Hello", "Apr", "IF1"),
        ImageItem(12, "Hello", "May", "IF2"),
        ImageItem(13, "Hello", "Jun", "IF3"),
        RecordItem(21, "Bonjour", "Jul", "RF1"),
        RecordItem(22, "Bonjour", "Aug", "RF2"),
        RecordItem(23, "Bonjour", "Sep", "RF3")
    )

    for (i in items){
        println(i.getDetails())
    }
}*/

fun showItem(item: Item){
    println("Item: ${item.getDetails()}")
}

fun showImageItem(imageItem: ImageItem){
    println("ImageItem: ${imageItem.getDetails()}")
}

fun showRecordItem(recordItem: RecordItem){
    println("RecordItem: ${recordItem.getDetails()}")
}

//if...else if ...else

fun showItem02(item: Item){
    if (item is ImageItem){
        println("ImageItem: ${item.getDetails()}")
    }
    else if (item is RecordItem){
        println("RecordItem: ${item.getDetails()}")
    }
    else {
        println("Item: ${item.getDetails()}")
    }
}

fun showItem03(item: Item){
    if (item is ImageItem){
        println("ImageItem: ${item.id}, ${item.imageFile}")
    }
    else if (item is RecordItem){
        println("RecordItem: ${item.id}, ${item.recordFile}")
    }
    else {
        println("Item: ${item.id}")
    }
}

//smart

fun showItem04(item: Item){
    when (item){
        is ImageItem ->
            println("ImageItem: ${item.getDetails()}")
        is RecordItem ->
            println("RecordItem: ${item.getDetails()}")
        else ->
            println("Item: ${item.getDetails()}")
    }
}

fun main(args: Array<String>){

    /*val i = Item(1, "Hello", "Taiwan")
    showItem(i)

    val ii = ImageItem(2, "Hello", "Japan", "Fuji.jpg")
    showImageItem(ii)

    val ri = RecordItem(3, "Bonjour", "Thailand", "Food.mp4")
    showRecordItem(ri)*/

    var items: Array<Item> = arrayOf(
        Item(1, "Hello", "Taiwan"),
        Item(2, "Hello", "HsinChu"),
        Item(3, "Hello", "Taipei"),
        ImageItem(11, "Hi", "Thailand", "Food.jpg"),
        ImageItem(12, "Hi", "India", "Temple.jpg"),
        ImageItem(13, "Hi", "Hong Kong", "Night View.jpg"),
        RecordItem(21, "Bonjour", "UK", "Gentelman.mp4"),
        RecordItem(22, "Bonjour", "US", "Maroon 5.mp4"),
        RecordItem(23, "Bonjour", "Australia", "Mars.mp4")
    )

    for (i in items){
        showItem04(i)
    }
}