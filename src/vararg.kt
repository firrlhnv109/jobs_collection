package src.vararg

/*fun average(vararg ns: Int): Int{
    var total = 0

    for (n in ns){
        total += n
    }

    return total / ns.size
}

fun main(args: Array<String>){
    println(average(1,2,3))
    println(average(1,2,3,4,5))
}*/

fun average03(vararg ns: Int, message: String){
    var total = 0

    for (n in ns){
        total += n
    }

    val average = total / ns.size

    println("$message: $average")
}

fun average04(message: String, vararg ns: Int){
    var total = 0

    for (n in ns){
        total += n
    }

    var avarage = total / ns.size

    println("$message: $avarage")
}

fun main(args: Array<String>){
    //println(average03(1,2,3, "Average"))
    average04("Average", 1,2,3,4,5,6,7)
}