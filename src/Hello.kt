package Jobs

fun getGreeting(greet: String, name: String): String{
    return greet + "," + name + "!"
}

fun getGreeting2(greet: String, name: String) =
        greet + "," + name + "!"

fun sayGreeting(greet: String, name: String): Unit{
    println(greet + "," + name + ":)")
}

fun sayGreeting2(greet: String, name: String){
    println(greet + "," + name + "!")
}

fun main(args: Array<String>) {
    print("Hello World!")
    println("I am handsome.")
    println(getGreeting("Hello","Taiwan"))
    println(getGreeting2("Hello","Hsinchu"))
    sayGreeting("Hello","Jobs")
    sayGreeting2("Hello","No.1")
}