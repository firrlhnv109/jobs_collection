package src.Class

//Class

/*class Item01 (val id: Long,
              var title: String,
              var content: String)

fun main(args: Array<String>){
    val item01 = Item01(1,"Hello", "Kotlin!")
    println("Item01(id=${item01.id}, title=${item01.title}, content=${item01.content})")

    item01.title = "Bye"
    item01.content = "Java!"

    println("Item01(id=${item01.id}, title=${item01.title}, content=${item01.content})")

}*/

//Class init

/*class Item02 (val id: Long = 0,
              var title: String = "",
              var content: String = ""){

    init {
        title = if (title.isEmpty()) "Title required." else title
        content = if (content.isEmpty()) "Content required." else content
    }

    fun getDetails() = "Item02(id=$id, title=$title, content=$content)"
}

fun main(args: Array<String>){
    val item0203 = Item02(203, "Hello", "Kotlin!")
    println(item0203.getDetails())

    val item0204 = Item02(204)
    println(item0204.getDetails())
}*/

//Class Array

/*class Item02 (val id: Long = 0,
              var title: String = "",
              var content: String =""){
    fun getDetails() = "Item02(id=$id, title=$title, content=$content)"
}

fun main(args: Array<String>){
    val items = arrayOf(
        Item02(2001),
        Item02(2002),
        Item02(2003)
    )

    for (item in items){
        println(item.getDetails())
    }

    println()

    for (item in items){
        item.title = "Hi"
        item.content = "Kotlin!"
        println(item.getDetails())
    }
}*/

//Class This

/*class Item03 (val id: Long,
              var title: String,
              var content: String){

    constructor(id: Long): this(id, "Title required.", "Content required.")
    fun getDetails() = "id=$id, title=$title, content=$content"
}

fun main(args: Array<String>){
    val item0301 = Item03(301, "Hello", "Kotlin!")
    println(item0301.getDetails())

    val item0302 = Item03(302)
    println(item0302.getDetails())
}*/

//Class set, get

/*class Item05 (val id: Long,
              _title: String,
              _content: String = ""){

    var title = _title

    set(value: String){
        if (value.isNotEmpty()){
            field = value
        }
    }

    var content = _content

    get(){
        return if (field.isEmpty()) "Empty" else field
    }

    fun getDetails() = "Item05(id=$id, title=$title, content=$content)"
}

fun main(args: Array<String>){
    val item05 = Item05(5,"Nice day")
    println(item05.getDetails())

    item05.title = ""
    println("title=${item05.title}")

    item05.content = ""
    println("content=${item05.content}")

    item05.content = "Kotlin!"
    println("content=${item05.content}")
}*/

//Class open, override, super

open class Item (val id: Long,
                 var title: String,
                 var content: String){

    fun getReduceContent(length: Int = 5) = "${content.substring(0 until length)}"

    open fun getDetails() = "id=$id, title=$title, content=$content"
}

class ImageItem (id: Long,
                 title: String,
                 content: String,
                 var imageFile: String): Item(id, title, content){

    /*override fun getDetails() = "id=$id, title=$title, " +
            "content=$content, imageFile=$imageFile"*/
    override fun getDetails() = "${super.getDetails()}, imageFile=$imageFile"
}

fun main(args: Array<String>){
    val i = Item(1, "Hello", "Hello Kotlin!")
    println(i.getDetails())
    println(i.getReduceContent())

    val i02 = ImageItem(2, "Hi", "Good morning", "kotlin.jpg")
    println(i02.getDetails())
    println(i02.getReduceContent(6))
}