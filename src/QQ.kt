fun main(args: Array<String>) {
    var i: Int = 1
    var a: Int = 1
    var j: Int = 1

    print("Enter tree layer: ")
    val stringInput: Int = readLine()!!.toInt()

    for ( i in 1..stringInput ){
        for ( j in stringInput downTo i ){
            print(" ")
        }
        for ( a in 1..i ){
            print("*")
        }
        println()
    }
}

